# Servicio consulta SQLServer para Web Nacional Seguros

V1.1.7

## Introducción

Este servicio es un version reducida y compilada de SqlServerToRest

- permite la consulta de una vista
- filtra los resultados por cada ruta(Filtros configurados dentro de la compilación)
- devuelve los datos deacuerdo a la estructura para contruir tablas en la pagina Web

### Rutas disponibles

- (GET) /data/red-medicas(alias: /data/red-medica)

- (GET) /data/red-oficinas

- (GET) /data/red-talleres

- (GET) /data/red-pagos

### Respuesta de cada servicio

```json
{
  "table": {
    "headers": [
      {
        "text": "Nombre", // Nombre de columna
        "value": "name" // Valor de columna
      }
    ],
    "rows": [
      {
        "name": "Hola mundo"
      }
    ]
  }
}
```

## Cache

El servicion cuenta con un sistema de cache para evitar la sobrecarga de consultas a la base de datos este se puede parametrizar en con la variable de entorno CACHE

## Seguridad

Para produccion se habilita por defecto JsonWebToken, el token esta firmado por una clave interna que set puede cambiar por la variable JWT_SECRET, igualmente se pude desabilitar la seguridad colocando JWT con un valor igual a OFF

- No se recomienda por el momento cambiar JWT_SECRET

## Variables de entorno

| Nombre            | Descripcion                                                   |
| ----------------- | ------------------------------------------------------------- |
| PORT              | Puerto de servicio                                            |
| NODE_ENV\*        | Modo de despliege de servicio                                 |
| DB_QUERY\*        | Query de consulta                                             |
| DB_USER\*         | Usuario del servidor base de datos                            |
| DB_PASSWORD\*     | Password del servidor base de datos                           |
| DB_SERVER\*       | Endpoint delServidor de la base de datos                      |
| DB_DATABASE\*     | Nombre de la base de datos a consultar                        |
| DB_ENABLE_ABORT\* | Abortar conexion                                              |
| CACHE             | Cache de cosulta(en Milisegundos) - por defecto 600000(10min) |
| JWT\*\*           | Hapilitar o desavilitarJwt secure auth                        |
| JWT_SECRET\*\*    | Clave secreta de JWT                                          |

\* Campos obligatorios

\*\* Valores por defecto en JWT=ON y JWT_SECRET=clave secreta interna, Notificar si se cambion esto parametros

## Deploy

Creación de la imagen

```
docker build ./ -t mk-web-service-data
```

Ejecución del contenedor

```
docker-compose up -d
```

# Servicio de consultas SqlServerToRest (FullService)

## Introducción

Este es un plugin diseñado para [Hapi.js](https://hapi.dev/) que convierte un archivo .json a una serie de rutas REST para consultas SQL de solo lectura

## Ejemplo

### Archivo de configuración

Este es el archivo que se configura para exponer las rutas en el servicio

routes.json

```json
[
  {
    "id": "users",
    "query": "select * from users",
    "path": "/data/users",
    "method": "get",
    "serializeData": [
      {
        "value": "email",
        "originValue": "<% CorreoElectronico %>"
      },
      {
        "value": "fullname",
        "originValue": "<% PrimerNombre %> <% Segundo nombre %>"
      }
    ]
  },
  {
    "id": "groups",
    "query": "select * from groups",
    "path": "/data/groups",
    "method": "get",
    "serializeData": [
      {
        "value": "id",
        "originValue": "<% Id %>"
      },
      {
        "value": "name",
        "originValue": "<% Nombre %>"
      }
    ]
  }
]
```

### Servicio

El servicio expone las siquietes rutas:

- (GET)/data/users
- (GET)/data/groups

Respuesta de servicio al consultar (GET)/data/users

```json
[
  {
    "fullname": "Julio Tobar",
    "email": "email@email.com"
  }
]
```

## Más información

Contactar con el departamento de desarrollo de MonkeyPlus:

- Andrés Acosto - aacosto@monkeyplusbc.com (Jefe de departamento)
- Andr­és Navarrete - anavarrete@monkeyplusbc.com (Desarrollador del proyecto)
