# Cambios

## v1.0.1

- Agregar fila Ciudad a red de talleres

## v1.0.2

### Error

- Posible error al ejecutar el servicio, se requiere la variable de entorno NODE_OPTIONS

## v1.1.0

- Retorno de meta informacion en los headers de respuesta
- /data/red-medicas alias /data/red-medica

## v1.1.1

- Agregar campos: dsPlanHabilitado,dsDistincion

## v1.1.2

- Correccion texto "Especialdad" a "Especialidad"

## v1.1.3

- Inclusion de campo "Departamento" en red Talleres

## v1.1.4
- Agregar campos: cdSubEspecialidad,SubEspecialidad,dsRed

## v1.1.5
- Agregar campos: latitude,longitude,dsCategoriaCentroMedico

## v1.1.6
- Agregar campos: Atencion

## v1.1.7
- Cambio de campo ciudad
