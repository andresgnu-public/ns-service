FROM node:12

WORKDIR /app

RUN npm install pm2 -g

COPY . . 

RUN chmod 775 service

EXPOSE 3000

ENV PORT="3000"
ENV NODE_ENV="production"
CMD [ "pm2-runtime", "ecosystem.config.js" ]
